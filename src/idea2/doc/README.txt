========
当前目录下的代码是基于DeepLearnToolBox修改而来；
1）采用常规的卷积算法；
2）最后一层采用max-pooling，从让变长的句子，产生相同长度的效果；


创新点：
1）损失函数的设计
2）无监督变长文本向量的学习

基本假设：
1）让学习到的变长文本向量在每一个词向量上的投影尽可能大；

Cost Function的构造：
1）基础原理是最大似然；
2）根本问题是在满足基本假设的情况下构造概率函数。只需要让学习到的向量在词向量的上投影除以
该词向量的范数从而得到概率，这样就构造了概率。