function net = cnnsetup(net)
    disp('-------------------Begin setting up cnn--------------------');
    % Display CNN Structure.
    disp('CNN Structure:');
    for d = 1:numel(net.layers)
        disp(net.layers{d});
    end
    % Display Free Parameters.
    disp('CNN Free Parameters:');

    n = numel(net.layers);
    map_row_num = net.word_dim;
    for l = 1 : n   %  layer
        if strcmp(net.layers{l}.type, 'c')
            for j = 1 : net.layers{l}.outputmaps  %  output map
                for i = 1 : net.layers{l - 1}.outputmaps  %  input map
                    net.layers{l}.k{i}{j} = normrnd(0, 0.05, net.layers{l}.kernelsize);
                end
                net.layers{l}.b{j} = 0;
            end
            map_row_num = map_row_num - net.layers{l}.kernelsize + 1;
        end
    end
    map_col_num = net.top_pooling_width;
    disp(['map_size:[' num2str(map_row_num) ', ' num2str(map_col_num) ']'])

    % 'onum' is the number of labels, that's why it is calculated using size(y, 1). If you have 20 labels so the output of the network will be 20 neurons.
    % 'fvnum' is the number of output neurons at the last layer, the layer just before the output layer.
    % 'ffb' is the biases of the output neurons.
    % 'ffW' is the weights between the last layer and the output neurons. Note that the last layer is fully connected to the output layer, that's why the size of the weights is (onum * fvnum)
    fvnum = map_row_num * map_col_num * net.layers{n}.outputmaps;
    onum = net.sent_dim;

    disp(['fvnum:' num2str(fvnum) ', onum:' num2str(onum)]);

    net.ffb = zeros(onum, 1);
    net.ffW = (rand(onum, fvnum) - 0.5) * 2 * sqrt(6 / (onum + fvnum));

    disp(['layer' num2str(n) '(full)' ' weight parameters: ffW[' num2str(size(net.ffW)) ']    ffb[' num2str(size(net.ffb)) ']']);

    disp('-------------------End setting up cnn--------------------');
end
