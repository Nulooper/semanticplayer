function grad = grad_sigmoid(z)
	grad = z .* (1 - z);
end