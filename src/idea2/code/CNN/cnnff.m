function net = cnnff(net, x)
    % x: a sentence matrix, [word_dim, sent_length]

    n = numel(net.layers);
    sent_length = size(x, 2);
    net.layers{1}.a{1} = x;
    
    cur_conv_index = 1;

    for l = 2 : n   %  for each layer
        if strcmp(net.layers{l}.type, 'c')
            %  !!below can probably be handled by insane matrix operations
            for j = 1 : net.layers{l}.outputmaps   %  for each output map
                %  create temp output map
                [row, col] = size(net.layers{l - 1}.a{1});
                z = zeros(row - net.layers{l}.kernelsize + 1, col - net.layers{l}.kernelsize + 1);
                for i = 1 : net.layers{l - 1}.outputmaps   %  for each input map
                    %  convolve with corresponding kernel and add to temp output map
                    z = z + convn(net.layers{l - 1}.a{i}, net.layers{l}.k{i}{j}, 'valid');
                end
                %  add bias, pass through nonlinearity
                z = z + net.layers{l}.b{j};
                net.layers{l}.a{j} = check_act_func(z, net.layers{l}.act_func);
            end

        elseif strcmp(net.layers{l}.type, 's')
            %  downsample
            d_k = compute_dynamic_k(net, cur_conv_index, sent_length);
            for j = 1 : net.layers{l - 1}.outputmaps
                [net.layers{l}.a{j}, net.layers{l}.k_indices{j}] = k_max_pooling(net.layers{l-1}.a{j}, d_k);
            end
            cur_conv_index = cur_conv_index + 1;
        end
    end

    %  concatenate all end subsample layer feature maps into vector
    net.fv = [];
    for j = 1 : numel(net.layers{n}.a)
        sa = size(net.layers{n}.a{j});
        net.fv = [net.fv; reshape(net.layers{n}.a{j}, sa(1) * sa(2), 1)];
    end
    %  feedforward into output perceptrons
    net.o = sigm(net.ffW * net.fv + net.ffb);

end
