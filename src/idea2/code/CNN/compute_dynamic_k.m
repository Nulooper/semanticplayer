function k = compute_dynamic_k(cnn, cur_conv_index, cur_sent_length)
	d_k = floor((cnn.total_conv_num - cur_conv_index)/cnn.total_conv_num * cur_sent_length);
	
    if cur_conv_index == cnn.total_conv_num
        k = cnn.top_pooling_width;
    else
        k = max([cnn.top_pooling_width d_k]);
    end
end