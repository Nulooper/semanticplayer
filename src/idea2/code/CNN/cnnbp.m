function net = cnnbp(net, x)
    n = numel(net.layers);

    %   error
    length = size(x, 2);
    tmp = 1 ./ (x'*net.o);
    net.e = zeros(net.sent_dim, 1);
    for i = 1:net.sent_dim
        net.e(i,1) = sum(tmp.*x(i,:)');
    end
    
    %  loss function
    net.L = sum(log((1./sum(x.^2)) .* ((net.o)'*x) .*(1./[1:length]))) + log(1/length);

    %%  backprop deltas
    net.od = net.e .* (net.o .* (1 - net.o));   %  output delta
    net.fvd = (net.ffW' * net.od);              %  feature vector delta

    %  reshape feature vector deltas into output map style of the last subsample layer.
    sa = size(net.layers{n}.a{1});
    fvnum = sa(1) * sa(2);
    for j = 1 : numel(net.layers{n}.a)
        net.layers{n}.d{j} = reshape(net.fvd(((j - 1) * fvnum + 1) : j * fvnum), sa(1), sa(2));
    end

    for l = (n - 1) : -1 : 1
        if strcmp(net.layers{l}.type, 'c')
            [row, col] = size(net.layers{l}.a{1});
            for j = 1 : numel(net.layers{l}.a)
                k_indices = net.layers{l + 1}.k_indices{j};
                net.layers{l}.d{j} = zeros(row, col);
                for m = 1:row
                    for x = 1:size(k_indices, 2)
                        net.layers{l}.d{j}(m, k_indices(m, x)) = net.layers{l + 1}.d{j}(m, x);
                    end
                end
                net.layers{l}.d{j}(:,:) = net.layers{l}.d{j}(:,:).*check_grad(net.layers{l}.a{j}, net.layers{l}.act_func);
            end

        elseif strcmp(net.layers{l}.type, 's')
            for i = 1 : numel(net.layers{l}.a)
                z = zeros(size(net.layers{l}.a{1}));
                for j = 1 : numel(net.layers{l + 1}.a)
                     z = z + convn(net.layers{l + 1}.d{j}, rot180(net.layers{l + 1}.k{i}{j}), 'full');
                end
                net.layers{l}.d{i} = z;
            end
        end
    end

    %%  calc gradients
    for l = 2 : n
        if strcmp(net.layers{l}.type, 'c')
            for j = 1 : numel(net.layers{l}.a)
                for i = 1 : numel(net.layers{l - 1}.a)
                    net.layers{l}.dk{i}{j} = convn(flipall(net.layers{l - 1}.a{i}), net.layers{l}.d{j}, 'valid');
                end
                net.layers{l}.db{j} = sum(net.layers{l}.d{j}(:));
            end
        end
    end
    net.dffW = net.od * (net.fv)';
    net.dffb = net.od;

    function X = rot180(X)
        X = rot90(X,2);
    end
end
