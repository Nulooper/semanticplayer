function learned_sent_matrix = cnntest(net, x, x_len_vec)
    %  feedforward
    learned_sent_matrix = [];
    m = size(x, 3);
    for i = 1:m
    	net = cnnff(net, x(:,1:x_len_vec(i),i));
    	learned_sent_matrix = [learned_sent_matrix; net.o]
    end
end
