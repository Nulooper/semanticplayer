function test_cnn_gradients_are_numerically_correct
x = rand(5,14,200);
x_len_vec = ones(200,1)*12;
cnn.layers = {
    struct('type', 'i', 'outputmaps', 1) %input layer
    struct('type', 'c', 'outputmaps', 2, 'kernelsize', 2, 'act_func', 2) %convolution layer
    struct('type', 's', 'outputmaps', 2) %sub sampling layer
    struct('type', 'c', 'outputmaps', 4, 'kernelsize', 2, 'act_func', 2) %convolution layer
    struct('type', 's', 'outputmaps', 4) %subsampling layer
};

cnn.top_pooling_width = 2;
cnn.total_conv_num = 2;
cnn.sent_dim = 5;
cnn.word_dim = 5;


m = size(x, 3);
for i = 1:m
	disp(['Gradient Checking:' num2str(i)]);
	sentence = x(:,1:x_len_vec(i));
	cnn = cnnsetup(cnn);
	cnn = cnnff(cnn, sentence);
	cnn = cnnbp(cnn, sentence);
	cnnnumgradcheck(cnn, sentence);
end
