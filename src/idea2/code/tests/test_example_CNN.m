function test_example_CNN
%load mnist_uint8;

%train_x = double(reshape(train_x',28,28,60000))/255;
%test_x = double(reshape(test_x',28,28,10000))/255;
%train_y = double(train_y');
%test_y = double(test_y');

%% ex1 Train a 6c-2s-12c-2s Convolutional neural network 
%will run 1 epoch in about 200 second and get around 11% error. 
%With 100 epochs you'll get around 1.2% error

%rand('state',0)
x = rand(5,25,50);
x_len_vec = ones(50,1)*8;

cnn.layers = {
    struct('type', 'i', 'outputmaps', 1) %input layer
    struct('type', 'c', 'outputmaps', 6, 'kernelsize', 2, 'act_func', 1) %convolution layer
    struct('type', 's', 'outputmaps', 6) %sub sampling layer
    struct('type', 'c', 'outputmaps', 12, 'kernelsize', 2, 'act_func', 1) %convolution layer
    struct('type', 's', 'outputmaps', 12) %subsampling layer
};
cnn.top_pooling_width = 2;
cnn.total_conv_num = 2;
cnn.sent_dim = 5;
cnn.word_dim = 5;


opts.alpha = 1; % learning rate.
opts.batchsize = 50;
opts.numepochs = 10;
opts.lamda = 0.5; % weight decay term

cnn = cnnsetup(cnn);
org = cnn.ffW;
%disp(cnn.ffW);
cnn = cnntrain(cnn, x, x_len_vec, opts);
cur = cnn.ffW;
disp(cur - org);

%[er, bad] = cnntest(cnn, test_x, test_y);

%plot mean squared error
%figure; plot(cnn.rL);
%assert(er<0.12, 'Too big error');
