function test_example_dcnn

addpath(genpath('../../../datasets/p2s'))
addpath(genpath('./utils'))

load train_para_matrix;
load train_para_len_vec;

cnn.hp.word_dim = 50;
cnn.hp.sent_dim = 50;
cnn.hp.learning_rate = 0.05;
cnn.hp.max_sent_length = 301;
cnn.hp.is_gpu_on = 0;
cnn.hp.lamda = 0.02; % the weight of regularization term.
cnn.hp.batchsize = 1;
cnn.hp.top_pooling_width = 2;
cnn.hp.total_conv_num = 1;
cnn.hp.numepochs = 10;



cnn.layers = {
    struct('type', 'input', 'feature_maps', 1) %input layer

    % act_func: 0 for none, 1 for relu, 2 for sigmoid, 3 for tanh_opt
    struct('type', 'conv', 'feature_maps', 8, 'kernel_row', 1, 'kernel_col', 3, 'act_func', 2) 
    struct('type', 'sub', 'feature_maps', 8) % sub sampling layer

    % act_func: 0 for none, 1 for relu, 2 for sigmoid, 3 for tanh_opt
    %struct('type', 'conv', 'feature_maps', 6, 'kernel_row', 1, 'kernel_col', 2, 'act_func', 1) 
    %struct('type', 'sub', 'feature_maps', 6) % sub sampling layer
};

cnn = dcnnsetup(cnn);

org = cnn.layers{2}.W;

cnn = anotherTrain(cnn, train_para_matrix, train_para_len_vec);
cur = cnn.layers{2}.W
cur - org

save cnn;
