function cnn = cnnapplygrads(cnn, grad)
	n = numel(cnn.layers);
	
	for l = 2 : n - 1
		if strcmp(cnn.layers{l}.type, 'conv')
            cnn.layers{l}.W = cnn.layers{l}.W - cnn.hp.learning_rate * grad{l}.W;
            cnn.layers{l}.b = cnn.layers{l}.b - cnn.hp.learning_rate * grad{l}.b;
		end
	end 

	cnn.layers{n}.W = cnn.layers{n}.W - cnn.hp.learning_rate * grad{n}.W;
	cnn.layers{n}.b = cnn.layers{n}.b - cnn.hp.learning_rate * grad{n}.b;
end