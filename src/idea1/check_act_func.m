function result = check_act_func(X, act_func)
	if act_func == 0
        result = X;
    elseif act_func == 1
        result = relu(X);
    elseif act_func == 2
        result = sigmoid(X);
    elseif act_func == 3
        result = tanh_opt(X);
    end	
end