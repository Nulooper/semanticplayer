function cnn = cnnsetup(cnn)
    disp('-------------------Begin setting up cnn--------------------');

    % Display hyper parameters.
    disp('Hyper Parameters:');
    disp(cnn.hp);

    % Display CNN Structure.
    disp('CNN Structure:');
    for d = 1:numel(cnn.layers)
        disp(cnn.layers{d});
    end

    % Display Free Parameters.
    disp('CNN Free Parameters:');

    for l = 1 : numel(cnn.layers)   %  layer

        if strcmp(cnn.layers{l}.type, 'conv')
            cnn.layers{l}.W = normrnd(0,0.05,[cnn.hp.word_dim*cnn.layers{l}.kernel_col, cnn.layers{l-1}.feature_maps, cnn.layers{l}.feature_maps]);
            cnn.layers{l}.b = rand(cnn.hp.word_dim, cnn.layers{l}.feature_maps);

            disp(['layer' num2str(l) '(conv)' ' weight parameters: W[' num2str(size(cnn.layers{l}.W)) ']    b[' num2str(size(cnn.layers{l}.b)) ']']);
        end
    end
    
    % Initialize weights of the first full connection layer.
    cnn.layers{l + 1}.W = normrnd(0, 0.05, [cnn.hp.sent_dim, cnn.hp.word_dim*cnn.hp.top_pooling_width*cnn.layers{l}.feature_maps]);
    cnn.layers{l + 1}.b = rand(cnn.hp.sent_dim, 1);
    cnn.layers{l + 1}.type = 'full';

    disp(['layer' num2str(l+1) '(full)' ' weight parameters: W[' num2str(size(cnn.layers{l+1}.W)) ']    b[' num2str(size(cnn.layers{l+1}.b)) ']']);

    disp('-------------------End setting up cnn--------------------');
end
