function learn_vector

addpath(genpath('../tasks/p2s'))
addpath(genpath('./utils'))

load cnn

% learn train paragraph vector.
load train_para_len_vec;
load train_para_matrix;
m = size(train_para_len_vec); % number of sentences.
last_index = 0;
train_paragraph_vec = [];
for i = 1:m
	sentence = train_para_matrix(:, (last_index*301 + 1):(last_index*301 + train_para_len_vec(i)));
	sentence_length = train_para_len_vec(i);
	cnn = dcnnff(cnn, sentence, sentence_length);
	train_paragraph_vec = [train_paragraph_vec cnn.outputs];
	last_index = last_index + 1;
end
train_paragraph_vec = train_paragraph_vec';
save('train_para_vector.txt', 'train_paragraph_vec', '-ASCII')

disp('end 1');


% learn train sentence vector.
load train_sent_len_vec;
load train_sent_matrix;
m = size(train_sent_len_vec);
last_index = 0;
train_sentence_vec = [];
for i = 1:m
	sentence = train_sent_matrix(:, (last_index*301 + 1):(last_index*301 + train_sent_len_vec(i)));
	sentence_length = train_sent_len_vec(i);
	cnn = dcnnff(cnn, sentence, sentence_length);
	train_sentence_vec = [train_sentence_vec cnn.outputs];
	last_index = last_index + 1;
end
train_sentence_vec = train_sentence_vec';
save('train_sent_vector.txt', 'train_sentence_vec', '-ASCII')
disp('end 2');

% learn test paragraph vector.
load test_para_len_vec;
load test_para_matrix;
m = size(test_para_len_vec); % number of sentences.
last_index = 0;
test_paragraph_vec = [];
for i = 1:m
	sentence = test_para_matrix(:, (last_index*301 + 1):(last_index*301 + test_para_len_vec(i)));
	sentence_length = test_para_len_vec(i);
	cnn = dcnnff(cnn, sentence, sentence_length);
	test_paragraph_vec = [test_paragraph_vec cnn.outputs];
	last_index = last_index + 1;
end
test_paragraph_vec = test_paragraph_vec';
save('test_para_vector.txt', 'test_paragraph_vec' ,'-ASCII')
disp('end 3');

% learn test sentence vector.
load test_sent_len_vec;
load test_sent_matrix;
m = size(test_sent_len_vec);
last_index = 0;
test_sentence_vec = [];
for i = 1:m
	sentence = test_sent_matrix(:, (last_index*301 + 1):(last_index*301 + test_sent_len_vec(i)));
	sentence_length = test_sent_len_vec(i);
	cnn = dcnnff(cnn, sentence, sentence_length);
	test_sentence_vec = [test_sentence_vec cnn.outputs];
	last_index = last_index + 1;
end
test_sentence_vec = test_sentence_vec';
save ('test_sent_vector.txt' ,'test_sentence_vec' ,'-ASCII')
disp('end 4');

