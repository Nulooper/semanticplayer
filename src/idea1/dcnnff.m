function cnn = dcnnff(cnn, sentence, sent_length)
	sentence = sentence(:,1:sent_length);
	cur_conv_index = 1;

	n = numel(cnn.layers);
    cnn.layers{1}.a{1} = sentence;

    for l = 2 : n - 1
        if strcmp(cnn.layers{l}.type, 'conv')
        	for j = 1 : cnn.layers{l}.feature_maps
                z = zeros(size(cnn.layers{l-1}.a{1}, 1), size(cnn.layers{l-1}.a{1}, 2) + cnn.layers{l}.kernel_col - 1);
                for i = 1 : cnn.layers{l-1}.feature_maps
                    z = z + fastConv(cnn.layers{l - 1}.a{i}, fliplr(reshape(cnn.layers{l}.W(:, i, j), cnn.hp.word_dim, cnn.layers{l}.kernel_col)), 'f', cnn.hp.is_gpu_on);
                end
                z = z + repmat(cnn.layers{l}.b(:, j), 1, size(z, 2));
                cnn.layers{l}.a{j} = check_act_func(z, cnn.layers{l}.act_func);
            end
        

        elseif strcmp(cnn.layers{l}.type, 'sub')
            %  compute the k value of current convolution layer.
            d_k = compute_dynamic_k(cnn, cur_conv_index, sent_length);
            for j = 1 : cnn.layers{l}.feature_maps
                [cnn.layers{l}.a{j}, cnn.layers{l}.k_indices{j}] = k_max_pooling(cnn.layers{l-1}.a{j}, d_k);
            end
            cur_conv_index = cur_conv_index + 1;
        end
    end

    % Vectorize the last sample layer.
    cnn.last_sub_vec = [];
    for j = 1 : numel(cnn.layers{l}.a)
        [row col] = size(cnn.layers{l}.a{j});
        cnn.last_sub_vec = [cnn.last_sub_vec; reshape(cnn.layers{l}.a{j}, row*col, 1)];
    end

    %  feedforward into output.
    cnn.layers{n}.a{1} = sigmoid(cnn.layers{n}.W * cnn.last_sub_vec + cnn.layers{n}.b);

    % Assign return value.
    cnn.outputs = cnn.layers{n}.a{1};

    tmp_sum = [];
    for i = 1:sent_length
        tmp_sum = [tmp_sum sentence(:,i)./norm(sentence(:,i))];
    end
    cnn.L = -sum((sum(tmp_sum, 2)./sent_length.*cnn.outputs));
end