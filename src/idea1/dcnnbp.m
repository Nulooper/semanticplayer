function [cnn, grad] = dcnnbp(cnn, sentence, sent_length)
	sentence = sentence(:,1:sent_length);

	n = numel(cnn.layers);

	% full connection layer.
	tmp_diff = [];
	for i = 1:sent_length
		tmp_diff = [tmp_diff -sentence(:,i)/(norm(sentence(:,i))*sent_length)];
    end
    cnn.layers{n}.out_diff = sum(tmp_diff,2);
	cnn.layers{n}.error = cnn.layers{n}.out_diff.*(cnn.layers{n}.a{1}.*(1 - cnn.layers{n}.a{1})); 
	grad{n}.W =  cnn.layers{n}.error * (cnn.last_sub_vec)';
	grad{n}.b =  cnn.layers{n}.error;

		% Iterate to get error term.
	for i = (n - 1) : -1 : 2
        
		if strcmp(cnn.layers{i}.type, 'conv')
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Compute error term.
            [row, col] = size(cnn.layers{i}.a{1});
        	cnn.layers{i}.error = zeros(row, col, cnn.layers{i}.feature_maps);
			for j = 1:cnn.layers{i}.feature_maps
                k_indices = cnn.layers{i + 1}.k_indices{j};
        		for m = 1:row
        			for x = 1:size(k_indices, 2)
        				cnn.layers{i}.error(m, k_indices(m, x), j) = cnn.layers{i + 1}.error(m, x, j);
        			end
        		end
                cnn.layers{i}.error(:,:,j) = cnn.layers{i}.error(:,:,j).*check_grad(cnn.layers{i}.a{j}, cnn.layers{i}.act_func);     		
            end
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% Compute grad.
            grad{i}.W = zeros(cnn.hp.word_dim*cnn.layers{i}.kernel_col, cnn.layers{i - 1}.feature_maps, cnn.layers{i}.feature_maps);
			for j = 1:cnn.layers{i}.feature_maps
				
				for m = 1:cnn.layers{i - 1}.feature_maps
                    grad{i}.W(:, m, j) = reshape(fliplr(fastConv(cnn.layers{i}.error(:,:,j), fliplr(cnn.layers{i - 1}.a{m}), 'v', cnn.hp.is_gpu_on)), cnn.hp.word_dim*cnn.layers{i}.kernel_col, 1);   
				end
				grad{i}.b(:, j) = sum(cnn.layers{i}.error(:,:,j), 2);
            end
           
        elseif strcmp(cnn.layers{i}.type, 'sub')
        	%%%%%%%%%%%%%%%%%%%
        	% Compute error term.
        	if strcmp(cnn.layers{i + 1}.type, 'full')
        		% Compute error term.
            	cnn.layers{i}.out_diff = (cnn.layers{i + 1}.W)' * cnn.layers{i + 1}.error;
            	cnn.layers{i}.error = reshape(cnn.layers{i}.out_diff, cnn.hp.word_dim, cnn.hp.top_pooling_width, cnn.layers{i}.feature_maps);

            else
                [row, col] = size(cnn.layers{i}.a{1});
                cnn.layers{i}.error = zeros(row, col, cnn.layers{i}.feature_maps);
                disp(size(cnn.layers{i}.error));
            	for j = 1:cnn.layers{i}.feature_maps
            		for k = 1:cnn.layers{i + 1}.feature_maps
            			tmp_kernal = cnn.layers{i + 1}.W(:, j, k);
            			cnn.layers{i}.error(:,:,j) = cnn.layers{i}.error(:,:,j) + fastConv(cnn.layers{i + 1}.error(:,:,k), fliplr(reshape(tmp_kernal, cnn.hp.word_dim, cnn.layers{i+1}.kernel_col)), 'v', cnn.hp.is_gpu_on);
            		end
            	end
            end
        end	
    end
end