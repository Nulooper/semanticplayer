function k = compute_dynamic_k(cnn, cur_conv_index, cur_sent_length)
	d_k = floor((cnn.hp.total_conv_num - cur_conv_index)/cnn.hp.total_conv_num * cur_sent_length);
    if cur_conv_index == cnn.hp.total_conv_num
        k = cnn.hp.top_pooling_width;
    else
        k = max([cnn.hp.top_pooling_width d_k]);
    end
end