function grad = grad_relu(z)
	%grad = sigmoid(z);
	z(find(z <= 0)) = 0;
	z(find(z > 0)) = 1;
	grad = z;
end