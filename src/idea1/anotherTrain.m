function cnn = anotherTrain(cnn, sent_matrix, sent_len_vec)
	% sent_matrix: (word_dim, max_sent_length, sent_number)
	% sent_len_vec: (sent_number, 1)

	m = size(sent_matrix, 3); % number of sentences.
	numbatches = m / cnn.hp.batchsize;
	if rem(numbatches, 1) ~= 0
        error('numbatches not integer');
    end

    for i = 1 : cnn.hp.numepochs
        statistics{i}.cost = []; % for error analysis.
        disp(['epoch ' num2str(i) '/' num2str(cnn.hp.numepochs)]);
        tic;
        kk = randperm(m);
        for p = 1 : numbatches
            batch_sent_matrix = sent_matrix(:, :, (p - 1) * cnn.hp.batchsize + 1 : p * cnn.hp.batchsize);
            batch_sent_len_vec = sent_len_vec((p - 1) * cnn.hp.batchsize + 1 : p * cnn.hp.batchsize, 1);

            flag = 0;
            sum_grad{1}.W = zeros(0);
            sum_grad{1}.b = zeros(0);
            avg = 1 / cnn.hp.batchsize;
            stat_avg_cost = 0; 
            for s = 1:cnn.hp.batchsize
                cnn = dcnnff(cnn, batch_sent_matrix(:,:,s), batch_sent_len_vec(s, 1));
                stat_avg_cost = stat_avg_cost + cnn.L * avg;
                [cnn, grad] = dcnnbp(cnn, batch_sent_matrix(:,:,s), batch_sent_len_vec(s, 1));
                if flag == 0
                    % Initialize and accumulate the current grad.
                    for g = 1:numel(grad)
                        if sum(size(grad{g})) ~= 0 %%% Check g th cell is initialized.
                            sum_grad{g}.W = zeros(size(grad{g}.W));
                            sum_grad{g}.W = sum_grad{g}.W + grad{g}.W * avg;

                            sum_grad{g}.b = zeros(size(grad{g}.b));
                            sum_grad{g}.b = sum_grad{g}.b + grad{g}.b * avg;
                        end
                    end
                    flag = 1;
                else
                    % Accumulate grad.
                    for g = 1:numel(grad)
                        if sum(size(grad{g})) ~= 0
                            sum_grad{g}.W = sum_grad{g}.W + grad{g}.W * avg;
                            sum_grad{g}.b = sum_grad{g}.b + grad{g}.b * avg;
                        end
                    end
                end
            end
            disp(['epoch  ' num2str(i)  ' batch ' num2str(p)   'sent ' num2str(s) ':  stat_avg_cost: ' num2str(stat_avg_cost)]);
            cnn = dcnnapplygrads(cnn, sum_grad);

            statistics{i}.cost = [statistics{i}.cost; stat_avg_cost];
        end
        statistics{i}.avg = mean(statistics{i}.cost);
        toc;
    end

    % Plot cost figure. x-axis: epoch; y-axis: average cost of each epoch.
    x = 1:cnn.hp.numepochs;
    y = [];
    for i = 1:cnn.hp.numepochs
        y = [y; statistics{i}.avg]
    end
    plot(x, y);
end