function checkDCNNGradients()

	cnn.hp.word_dim = 5;
	cnn.hp.sent_dim = 5;
	cnn.hp.learning_rate = 0.025;
	cnn.hp.dropout_threshold = 0.2;
	cnn.hp.max_sent_length = 58;
	cnn.hp.max_epochs = 10;
	cnn.hp.is_gpu_on = 0;
	cnn.hp.windows_size = 3;
	cnn.hp.lamda = 0.02;
	cnn.hp.top_pooling_width = 2;
	cnn.hp.predict_hidden_num = 5;
	cnn.hp.total_conv_num = 1; 

	epsilon = 1e-4;
	depth_epsilon = kron(epsilon, ones(cnn.hp.word_dim, 1));
    er      = 1e-9;

	cnn.layers = {
    struct('type', 'input', 'feature_maps', 1) %input layer

    struct('type', 'conv', 'feature_maps', 6, 'kernel_row', 1, 'kernel_col', 2, 'act_func', 1)
    struct('type', 'sub', 'feature_maps', 6) 
    };

    cnn = dcnnsetup(cnn);
    n = numel(cnn.layers);

    sent_matrix = rand(5, 4, 4); 
    sent_len_vec = ones(4,1)*4;

    cnn = dcnnff(cnn, sent_matrix(:, :, 1), sent_len_vec(1));
    sent_vec = cnn.outputs;

    [cnn, cnn_grad] = dcnnbp(cnn, sent_matrix(:, :, 1), sent_len_vec(1));

    % Check graddients of full connection layers.
    
    % check bias.
    disp('Checking bias of full connection layers.');
    for j = 1 : numel(cnn.layers{n}.b)
    	cnn_m = cnn;
    	cnn_p = cnn;
    	cnn_m.layers{n}.b(j) = cnn_m.layers{n}.b(j) - epsilon;
    	cnn_p.layers{n}.b(j) = cnn_p.layers{n}.b(j) + epsilon;

    	cnn_m = dcnnff(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
    	cnn_p = dcnnff(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

    	cnn_m, cnn_m_grad = dcnnbp(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
    	cnn_p, cnn_p_grad = dcnnbp(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

    	d = (cnn_p.L - cnn_m.L) / (2 * epsilon);
    	e = abs(d - cnn_grad{n}.b(j));
    	disp(e);
    	if e > er
    		error('numerical gradient checking failed.');
    	end
    end	

    % check weights.
    disp('Checking weights of full connection layers.');
    for i = 1 : size(cnn.layers{n}.W, 1)
    	for u = 1 : size(cnn.layers{n}.W, 2)
    		cnn_m = cnn;
    		cnn_p = cnn;

    		cnn_p.layers{n}.W(i, u) = cnn_p.layers{n}.W(i, u) + epsilon;
    		cnn_m.layers{n}.W(i, u) = cnn_m.layers{n}.W(i, u) - epsilon;

    		cnn_m = dcnnff(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
    		cnn_p = dcnnff(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

    		[cnn_m, cnn_m_grad] = dcnnbp(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
    		[cnn_p, cnn_p_grad] = dcnnbp(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

    		d = (cnn_p.L - cnn_m.L) / (2 * epsilon);
    		e = abs(d - cnn_grad{n}.W(i, u));
            disp(e);
    		if e > er
    			error('numerical gradient checking failed.');
    		end
    	end
    end

    % Check gradients for convolution layers, as in our design, only convolution layers have weights and bias. 
    for l = n - 3 : -1 : 2
    	if strcmp(cnn.layers{l}.type, 'conv')
    		for j = 1 : numel(cnn.layers{l}.a)
    			cnn_m = cnn; cnn_p = cnn;
    			cnn_p.layers{l}.b(1, j) = cnn_p.layers{l}.b(1, j) + epsilon;
    			cnn_m.layers{l}.b(1, j) = cnn_m.layers{l}.b(1, j) - epsilon;

                cnn_m = dcnnff(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
                cnn_p = dcnnff(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

    			[cnn_m, cnn_m_grad] = dcnnbp(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
    			[cnn_p, cnn_p_grad] = dcnnbp(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

                d = (cnn_p.L - cnn_m.L) / (2 * epsilon);
                e = abs(d - cnn_grad{l}.b(1, j));
                disp(e);
                if e > er
                    error('numerical gradient checking failed.');
                end
                

    			for i = 1 : numel(cnn.layers{l - 1}.a)
    				for u = 1 : cnn.hp.word_dim
                        for v = 1 : cnn.layers{l}.kernel_col
                            cnn_m = cnn; cnn_p = cnn;
                            
                            tmp_m_weights = cnn_m.layers{l}.W(:, i, j);
                            tmp_m_weights = reshape(tmp_m_weights, cnn_m.hp.word_dim, cnn_m.layers{l}.kernel_col);
                            tmp_m_weights(u, v) = tmp_m_weights(u ,v) - epsilon;
                            cnn_m.layers{l}.W(:, i, j) = reshape(tmp_m_weights, cnn_m.hp.word_dim * cnn_m.layers{l}.kernel_col, 1);

                            tmp_p_weights = cnn_p.layers{l}.W(:, i, j);
                            tmp_p_weights = reshape(tmp_p_weights, cnn_p.hp.word_dim, cnn_p.layers{l}.kernel_col);
                            tmp_p_weights(u, v) = tmp_p_weights(u ,v) + epsilon;
                            cnn_p.layers{l}.W(:, i, j) = reshape(tmp_p_weights, cnn_p.hp.word_dim * cnn_p.layers{l}.kernel_col, 1);


                            cnn_m = dcnnff(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
                            cnn_p = dcnnff(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

                            [cnn_m, cnn_m_grad] = dcnnbp(cnn_m, sent_matrix(:, :, 1), sent_len_vec(1));
                            [cnn_p, cnn_p_grad] = dcnnbp(cnn_p, sent_matrix(:, :, 1), sent_len_vec(1));

                            d = (cnn_p.L - cnn_m.L) ./ (2 * epsilon);
                            
                            tmp_W = reshape(cnn_grad{l}.W(:, i, j), cnn.hp.word_dim, cnn.layers{l}.kernel_col);
                            e = abs(d - tmp_W(u, v));
                            
                            disp('e');
                            disp(e);
                            if e > er
                                error('numerical gradient checking failed.');
                            end
                        end
                    end
    			end
    		end
    	end
    end
end